# CLI

This is a utility for finding stales pages and emailing people about them.

TODO: archive command

## 1. Get stale pages for certain spaces

```sh
python ./stale.py --analyze ABC KEY MULT ./~1245
```
For personal spaces, use a dot-slash, e.g. `./~1245`

## 2. Create the mail merge

```sh
python ./stale.py --mailmerge
```

## 3. Send reminder email

```sh
python ./stale.py --send
```

## Use a different data dump folder

```sh
python ./stale.py -a ABC --data='todaysdump'
```


## Config

Create a `settings.cfg` (see `settings_example.cfg`)

```
[mail]
host=smtp.example.com
port=1337
encryption=starttls
sender=hello@example.com
password=asdfkjh43qitq...

[atlassian]
baseUrl=https://my_cloud_domain.atlassian.net
user=email@of_account.com
password=ksafheath43qihtp...
```

### Outlook details

Outlook.com > Settings ⚙ > Sync Email > Let devices and apps use POP

SMTP box has the server details to put in the cfg.

#### Create token

Profile pic > View Account > My Sign-ins > Security Info > Add Method > App Password

### Get Atlassian password/token

See https://support.atlassian.com/atlassian-account/docs/manage-api-tokens-for-your-atlassian-account/

