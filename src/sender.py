import smtplib
from traceback import format_exc
from .util import open_self, CONFIG
from pathlib import Path
import os
from email.message import EmailMessage

HOST=CONFIG.get('mail', 'host')
PORT=CONFIG.get('mail', 'port')
SENDER=CONFIG.get('mail', 'sender')
PASSWORD=CONFIG.get('mail', 'password', raw=True)
ENCRYPTION=CONFIG.get('server', 'encryption', fallback='starttls').lower()

smtp=None

def login():
  try:
    smtp = smtplib.SMTP(HOST, PORT)
    if (ENCRYPTION == 'starttls'):
      smtp.starttls()
    smtp.login(SENDER, PASSWORD)         
    print("Successfully logged in")
  except smtplib.SMTPException:
    print(format_exc())

def create_message(sender, subject, content):
  msg = EmailMessage()
  msg['From'] = sender
  msg['Subject'] = subject
  msg.add_header('Content-Type','text/html')
  msg.set_content(content, subtype='html')
  return msg

def send(sender, receivers, message):
  try:
    smtp.send_message(message, sender, receivers)
  except smtplib.SMTPException:
    print(format_exc())

def send_emails(emails):
  for n in os.listdir(emails):
    address = os.path.splitext(n)[0]
    with open(os.path.join(emails, n)) as file:
      send(SENDER, address,
        create_message(
          SENDER,
          'Confluence Cleanup',
          file.read()
          )
        )

# send_emails('_emails')