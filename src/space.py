# https://atlassian-python-api.readthedocs.io/confluence.html
import requests
import json
from pathlib import Path
from .util import CONFIG, BASEURL
from base64 import b64encode
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from collections import defaultdict
from traceback import format_exc

_t = CONFIG.get('atlassian', 'user')+':'+CONFIG.get('atlassian', 'password', raw=True)
TOKEN=b64encode(_t.encode('utf-8'))

HEADERS = {
   'Accept': 'application/json',
   'Authorization': 'Basic '+ str(TOKEN, 'utf-8')
}

YEAR = date.today().year
SIX_MONTHS_AGO = date.today() - relativedelta(months=+6)

stales=[]

# email : {
#     name: dafsfsaf,
#     spaces:{
#         LEO : [{id, title, lastUpdated}]
#         SPACEKEY: [{id, title, lastUpdated}]
#     }
# }
mailing_list={}

ownerless_pages={}

# spaceKey : {
#    name: spaceName
#    icon: relpath
#    owners: []
# }
space_owners={}

def REST_get(endpoint):
    try:
        r = requests.request('GET',BASEURL+endpoint,headers=HEADERS)
        return r.json()
    except:
        print(format_exc())
        return {}

def init_mail_entry(name, email, spaceKey):
    if not email in mailing_list:
        mailing_list[email] = {
            'name': name,
            'spaces':{}
        }
    if not spaceKey in mailing_list[email]['spaces']:
        mailing_list[email]['spaces'][spaceKey] = []

def add_mail_page(email, spaceKey, pageId, pageTitle, lastUpdated):
    mailing_list[email]['spaces'][spaceKey].append({'pageId':pageId, 'title':pageTitle, 'lastUpdated':lastUpdated})

# Check if date is older than 6 months, input Confluence date string
def stale_date(date):
    last_edited = datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.%fZ')
    if (last_edited.date() < SIX_MONTHS_AGO):
        return True
    return False

# Check if a page has comments older than 6 months:
def stale_comments(id):
    response = REST_get(f'/wiki/rest/api/content/{id}/descendant/comment?expand=history')
    dict=response['results']
    for comment in dict:
        if not stale_date(comment['history']['createdDate']):
            return False
    return True

def get_like_count(id):
    dicto = REST_get(f'/wiki/rest/likes/1.0/content/{id}/likes')
    return len(dicto['likes'])

# Returns a tuple (accid, isActive, email)
def owner(permmObj):
    oof = permmObj['subjects']['user']['results'][0]
    t = tuple([oof['accountId'], not '(Deactivated)' in oof['publicName'], oof['email']])
    return t

def get_space_owners(spaceKey):
    dicto = REST_get(f'/wiki/rest/api/space/{spaceKey}?expand=permissions,icon')
    dicto['permissions']
    space_admins = list(filter(
        lambda permm: (
            permm['operation']['operation'] == 'administer'
            and 'user' in permm['subjects']
            and not permm['subjects']['user']['results'][0]['accountType'] == 'app'),
        dicto['permissions']
    ))
    return {'name': dicto['name'], 'icon': dicto['icon']['path'], 'owners': list(map(owner, space_admins))}

def get_stale_pages(spaceKey, start=0, limit=25):
    dicto=REST_get(f'/wiki/rest/api/space/{spaceKey}/content/page?expand=history.lastUpdated&start={start}&limit={limit}')
    space_owners[spaceKey] = get_space_owners(spaceKey)
    for res in dicto['results']:
        if res['status'] != "current":
            continue
        id = res['id']
        date = res['history']['lastUpdated']['when']
        autho = res['history']['createdBy']
        if not stale_date(date) or not stale_comments(id):
            continue

        stales.append(id)
        if '(Deactivated)' in autho['publicName']:
            if not spaceKey in ownerless_pages:
                ownerless_pages[spaceKey] = []
            ownerless_pages[spaceKey].append({'pageId':res['id'], 'title':res['title'], 'lastUpdated':date})
        else:
            init_mail_entry(autho['publicName'], autho['email'], spaceKey)
            add_mail_page(
            autho['email'],
            spaceKey,
            id,
            res['title'],
            date
            )
    
    resultsCount = len(dicto['results'])
    if resultsCount < 25:
        print(f'Pages counted: {start+resultsCount}')
        return
    else:
        get_stale_pages(spaceKey, start+25, limit)

def get_last_updated_date(pageId):
    dicto=REST_get(f'/wiki/rest/api/content/{pageId}?expand=history.lastUpdated')
    return dicto['history']['lastUpdated']['when']

## ARCHIVE with to_be_archived.txt file
def archive_pages(file):
    to_archive = []
    with open(file) as f:
        ids = f.readlines()
    for pId in ids:
        if stale_date(get_last_updated_date(pId)):
            to_archive.append(pId)
    # chunk 
    # POST /wiki/rest/api/content/archive?pages=id1,id2
    # Need token permissions?
    endpoint = f'/wiki/rest/api/content/archive?pages={",".join(ids)}'
    r = requests.request('POST',BASEURL+endpoint,headers=HEADERS)

# -----------------------------------------------------------------------------------------
# |              Templating
# -----------------------------------------------------------------------------------------

from jinja2 import Environment, FileSystemLoader

def pretty_date(datestring):
    i = datetime.strptime(datestring, '%Y-%m-%dT%H:%M:%S.%fZ')
    o = i.strftime('%b %d, %Y')
    return o

j_env = Environment(loader=FileSystemLoader(Path(__file__).parent / "templates"))
j_env.filters['pretty_date'] = pretty_date
icon_base=BASEURL + '/wiki'

SpaceItem = j_env.get_template("space-item.html")
Email = j_env.get_template("email.html")

# mail_merge('_output/mail.json', '_output/owners.json', '_output/emails' )
def mail_merge(mail_file, owners_file, output_dir):
  with open(mail_file) as mfile:
    mailing_list = json.load(mfile)
  with open(owners_file) as ofile:
    SPACEOWNERS = json.load(ofile)
  for email, details in mailing_list.items():
    s = list(
      SpaceItem.render(
          baseUrl=BASEURL, spaceKey=spaceKey, spaceName=SPACEOWNERS[spaceKey]['name'], pages=pages
          )
      for spaceKey, pages in details['spaces'].items()
    )
    with open(f'./{output_dir}/{email}.html', 'w', encoding='utf-8') as output:
      output.writelines(
          Email.render(year=YEAR, content="<br/>".join(s))
          )