import pathlib
from configparser import ConfigParser

def open_self(rel):
  return open(pathlib.Path(__file__).parent / rel)

CONFIG = ConfigParser()
CONFIG.read_file(open_self('../settings.cfg'))
BASEURL=CONFIG.get('atlassian', 'baseUrl')
