import click
from traceback import format_exc
from os.path import join, exists
from os import mkdir
import json
from src import space, sender

TYPE_DIR=click.Path(file_okay=False)

# @click.option('--all', help='Analyze all spaces.')
@click.command('spaces')
@click.argument('spacekeys', nargs=-1)
@click.option('--analyze','-a', help='Get stale pages', is_flag=True)
@click.option('--data','-d', default='_output', help='Output data directory', type=TYPE_DIR)
@click.option('--mailmerge','-m', help='Generate emails', is_flag=True)
@click.option('--send','-s', help='Send emails generated by mailmerge', is_flag=True)
def main(spacekeys, analyze, data, mailmerge, send):
    """Get all stale pages from the space."""
    if not exists(data):
        mkdir(data)
    if (analyze):
        for sKey in spacekeys:
            click.secho(f'Analyzing {sKey}...', fg='magenta')
            space.get_stale_pages(sKey)
        
        mail = join(data, 'mail.json')
        with open(mail, 'w') as mlist:
            mlist.write(json.dumps(space.mailing_list))
            click.secho(f"Wrote to {mail}", fg='green')
        
        owners = join(data, 'owners.json')
        with open(owners, 'w') as olist:
            olist.write(json.dumps(space.space_owners))
            click.secho(f"Wrote to {owners}", fg='green')

        ownerless = join(data, 'ownerless.json')
        with open(ownerless, 'w') as plist:
            plist.write(json.dumps(space.ownerless_pages))
            click.secho(f"Wrote to {ownerless}", fg='green')

        stales = join(data, 'to_be_archived.txt')
        with open(stales, 'w') as slist:
            slist.writelines(space.stales)
            click.secho(f"All stale page IDs in {stales}", fg='green')
    
    epath = join(data, '_emails')
    if mailmerge:
        if not exists(epath):
            mkdir(epath)
        space.mail_merge(
            join(data, 'mail.json'),
            join(data, 'owners.json'),
            epath
        )
        click.secho(f"Created mailmerge files in {epath}", fg='blue')
    if send:
        try:
            sender.login()
            click.secho(f"Attempting to send emails in {epath}...", fg='blue')
            sender.send_emails(epath)
        except:
            click.secho(f"ERROR: {format_exc()}", fg='red')


if __name__ == '__main__':
    main()
